\mode<presentation>

\RequirePackage{xspace}
\DeclareRobustCommand*\email[1]{\href{mailto:#1}{\nolinkurl{#1}}}

\newcount\sepbar@count
\DeclareRobustCommand*\sepbar[1][1]{%
    \begingroup%
        \sepbar@count=\z@%
        \@whilenum\sepbar@count<#1\do{%
            \,\rule[-0.25\baselineskip]{0.25pt}{\baselineskip}%
            \advance\sepbar@count\@ne
        }%
    \endgroup%
    \,\xspace%
}

% Lancaster University colours
\RequirePackage{xcolor}
\definecolor{lancs-red}{RGB}{180,17,26}
\definecolor{lancs-grey}{RGB}{129,138,143}
\definecolor{lancs-black}{RGB}{33,29,24}

\setbeamercolor*{title}{fg=lancs-red}
\setbeamercolor*{date}{fg=lancs-black!40!lancs-grey}
\setbeamercolor*{author}{fg=lancs-black!40!lancs-grey}
\setbeamercolor*{frametitle}{fg=lancs-red}
\setbeamercolor*{normal text}{fg=lancs-black}
\setbeamercolor*{alerted text}{fg=lancs-red}
\setbeamercolor*{head/foot}{fg=lancs-grey!80}
\setbeamercolor*{section in head/foot}{fg=head/foot.fg}

\setbeamerfont*{title}{size=\huge}
\setbeamerfont*{subtitle}{size=\large}
\setbeamerfont*{date}{size=\normalsize}
\setbeamerfont*{author}{size=\normalsize}
\setbeamerfont*{frametitle}{size=\Large}
\setbeamerfont*{framesubtitle}{size=\large,series=\itshape}

\setbeamercovered{transparent}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]

\RequirePackage{calc}

\newlength\frame@margin\setlength\frame@margin{15.5pt}
\newlength\frame@width\setlength\frame@width{\paperwidth-2\frame@margin}
\newlength\frame@height\setlength\frame@height{\paperheight-2\frame@margin}
\newlength\maxContentHeight\setlength\maxContentHeight{0.83\frame@height}

\setbeamersize{text margin left=1.4\frame@margin, text margin right=1.4\frame@margin}

% background
\RequirePackage{tikz}
\usetikzlibrary{calc}
\RequirePackage{xifthen}
\newsavebox\lancs@logo
\savebox\lancs@logo{\includegraphics{lancs-logo}}
\newsavebox\lancs@logo@header
\savebox\lancs@logo@header{\includegraphics[width=0.175\paperwidth]{lancs-logo}}
\newlength\red@ht\setlength\red@ht{34.5pt}
\newlength\black@ht\setlength\black@ht{4.9pt}
\newlength\grey@ht\setlength\grey@ht{75.3pt}
\setbeamertemplate{background}{%
    \begin{tikzpicture}
        \node[anchor=north east,inner sep=0.3\ht\lancs@logo@header]
            at (current page.north east) {\usebox\lancs@logo@header};

        \useasboundingbox (0,0) rectangle (\paperwidth,\paperheight);
        \ifthenelse{\equal\beamer@againname{title}%
                    \OR\equal\beamer@againname{questions}%
                    \OR\equal\beamer@againname{discussion}}{%
            \node[anchor=south west, rectangle,%
                    minimum width=\frame@width,%
                    minimum height=\frame@height]
                (area) at (\frame@margin,\frame@margin) {};

            \coordinate (curve start) at ($(area.south east)-(\red@ht,0)$);
            \coordinate (curve top left) at ($(area.south west)+(0,\red@ht)$);
            \coordinate (black top right) at ($(area.south east |- curve top left)+(0,\black@ht)$);
            \coordinate (grey top left) at ($(area.south west |- black top right)+(0,\grey@ht)$);

            \fill[color=lancs-red] (area.south west) -- (curve start)
                arc (-90:0:\red@ht) -- (curve top left);
            \fill[color=lancs-black] (curve top left) rectangle (black top right);
            \fill[color=lancs-grey!15] (black top right) rectangle (grey top left);

        }{%else
            \fill[color=lancs-red] (current page.south east)
                -- ++(-\red@ht,0) arc (-90:0:\red@ht);
        }
    \end{tikzpicture}
}

% title page layout
\setbeamertemplate{title page}{%
    \begin{beamercolorbox}{title page header}
        \usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle\par
        \usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par
    \end{beamercolorbox}
    \vspace{1em}
    \begin{beamercolorbox}{date}
        \usebeamerfont{date}\usebeamercolor[fg]{date}\insertdate\par
    \end{beamercolorbox}
    \begin{beamercolorbox}{author}
        \usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor\par
    \end{beamercolorbox}
    \vfill
}

\setbeamertemplate{frametitle}{%
    \vspace{\ht\lancs@logo@header}
    \begin{beamercolorbox}{frametitle}
        \insertframetitle\par
        \ifx\insertframesubtitle\@empty
           \vspace{1em}\par
        \else
            \usebeamerfont{framesubtitle}\usebeamercolor{framesubtitle}\insertframesubtitle\par
        \fi
    \end{beamercolorbox}
    \vspace{-0.5em}\color{lancs-grey}\hrulefill
}

\newlength\headline@width
\setlength\headline@width{\paperwidth-\wd\lancs@logo@header-0.6\ht\lancs@logo@header}
\setbeamertemplate{headline}{%
    \vspace{1ex}%
    \begin{beamercolorbox}[ht=2.25ex,dp=1ex]{section in head/foot}%
        \insertnavigation\headline@width{}{}
    \end{beamercolorbox}
    \vspace{-4.25ex}
}

\setbeamertemplate{footline}{%
    \begin{beamercolorbox}[ht=2.25ex,dp=1ex]{head/foot}%
        \hspace{1em}
        \begingroup
            \usebeamercolor[fg]{frame number in head/foot}%
            \usebeamerfont{frame number in head/foot}%
            \insertpagenumber\,/\,\insertpresentationendpage\kern1em%
        \endgroup\sepbar{}\hspace{1.5ex}%
        \begingroup
            \usebeamerfont{date in head/foot}%
            \usebeamercolor{date in head/foot}%
            \insertshortdate%
        \endgroup\hspace{1.5ex}\sepbar{}\hspace{1.5ex}%
        \begingroup
            \usebeamerfont{author in head/foot}%
            \usebeamercolor{author in head/foot}%
            \insertshortauthor%
        \endgroup\hspace{1.5ex}\sepbar{}\hspace{1.5ex}%
        \begingroup
            \usebeamerfont{title in head/foot}%
            \usebeamercolor{title in head/foot}%
            \insertshorttitle%
        \endgroup
        \hfill\vspace{1pt}
    \end{beamercolorbox}
}

\mode<all>
